# -*- coding: utf-8 -*-
from unittest import TestCase

import directory_group_writeable2 as dgw
import re

class Dtest(TestCase):

    def setUp(self):
        self.ARG1UNDERSCORED = 'anyhpc_zztest1'
        self.ARG1SINGLEWORD = 'anyhpczztest1singleword'
	self.ARG1CONTAININGPIPE = 'anyhpc|fgrep'
	self.ARGSCONTAINPIPE = ['anyhpc','|','fgrep y']
	# Next we define something we will pass to linux date for fail
	self.DATEARGSDASHED = ['--date','01-01-1970']
	self.DATEBIN = '/usr/bin/date'
	self.DATEARGSSINGLESPACE = ['--date',' ']
	self.ARGLIST3MKDIR = ['-m', dgw.CHMOD_UNPREFIXED, 'anyhpc_zztest1']
	self.ARGLIST3MKDIR_STRING = '-m 2770 anyhpc_zztest1'


    def test_joined_from_list(self):

	joined_str = dgw.joined_from_list(self.ARGLIST3MKDIR)
	self.assertEqual(joined_str,self.ARGLIST3MKDIR_STRING)


    def test_args_valid(self):

	val1bool = dgw.args_valid([self.ARG1UNDERSCORED])
	self.assertTrue(val1bool)
	val2bool = dgw.args_valid([self.ARG1SINGLEWORD])
	self.assertTrue(val2bool)
	val3bool = dgw.args_valid([self.ARG1CONTAININGPIPE])
	self.assertFalse(val3bool)
	val4bool = dgw.args_valid(self.ARGSCONTAINPIPE)
	self.assertFalse(val4bool)


    def test_subout4match(self):
	# subout4match(command_without_args, args=[], argnum=0, arg_regex=None)
        tup5 = dgw.subout4match(self.DATEBIN,self.DATEARGSDASHED,0,re.compile('\d+'))
        self.assertGreater(tup5[0],0)
        tup5 = dgw.subout4match(self.DATEBIN,self.DATEARGSSINGLESPACE,1,re.compile(r'\d+'))
        self.assertEqual(tup5[0],204)
        tup5 = dgw.subout4match(self.DATEBIN,self.DATEARGSSINGLESPACE,1,re.compile(r'.*'))
	#print(tup5)
        self.assertGreater(tup5[0],0)

