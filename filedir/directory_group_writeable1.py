#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2017, Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

# python ./directory_group_writeable1.py anyhpc_zzpro101
# For much feedback on error before running execute export PYVERBOSITY=2

from os import chmod
from os import getenv as osgetenv
from os import path as ospath
#from os import stat as osstat
from os import sep
from sys import argv,exit
#from time import time
#import logging
import grp
from string import punctuation
from subprocess import Popen,PIPE
import shlex
import re
#from __future__ import with_statement

__version__ = '0.1'

PYVERBOSITY_STRING = osgetenv('PYVERBOSITY')
PYVERBOSITY = 1
if PYVERBOSITY_STRING is None:
	pass
else:
	try:
		PYVERBOSITY = int(PYVERBOSITY_STRING)
	except:
		pass
#print(PYVERBOSITY)
BUFSIZE1=4096
TABBY=chr(9)
UNDERSCORE=chr(95)
COLON=chr(58)
PARENTH_OPEN=chr(40)
PARENTH_CLOSE=chr(41)
DQ=chr(34)
SQ=chr(39)
PIPEY=chr(124)
CHMOD2=02770
#CHMOD3=0o2770

RE_NUMERIC_MARKER = re.compile('\d')

RE_ALPHA_UNDERSCORE = re.compile('[a-zA-Z]+{0}'.format(UNDERSCORE))
#RE_USERS_PID = re.compile(r"users{0}\{1}\{1}{2}.*{2},pid=".format(COLON,PARENTH_OPEN,DQ))
#RE_USERS_CSV = re.compile(r"users{0}{1}{1}<>{2}{2}".format(COLON,PARENTH_OPEN,PARENTH_CLOSE))

CMD_MKDIR='/usr/bin/mkdir'
CMD_CHGRP='/usr/bin/chgrp'
CHGRP_DEREF='--no-dereference'

STRING_SUBOUT_ARGS_INVALID='Args list for subout is not valid - does it contain pipe (|)?'
STRING_NTH_ARG_REGEX_FAILED_TEMPLATE="Arg {0} did not pass the required regex test!"
STRING_NONZERO_RC_TEMPLATE="Command {0} gave non-zero returncode rc={1}"


def joined_from_list(arglist,joinstr=' '):
	return joinstr.join([str(arg) for arg in arglist])


def isfile_and_positive_size(target_path_given):
    target_path = target_path_given.strip()
    if not ospath.isfile(target_path):
        return False
    if not osstat(target_path).st_size:
        return False
    return True


def args_valid(args_list):
	pipe_count = 0
	for arg in args_list:
		try:
			if PIPEY in arg:
				pipe_count+=1
				break
		except TypeError:
			pass

	return (pipe_count < 1)


def subout4(command_without_args, args=[], argnum=0, arg_regex=None):
	""" Suffix of 4 as we accept four args generally.
	cmdline should NOT contain any chr(124) pipe symbols (|)
	"""
	command_with_args = "{0} {1}".format(command_without_args,joined_from_list(args,' '))
	s_rc = 0
	cmdproc = Popen(shlex.split(command_with_args),bufsize=BUFSIZE1, \
		stdout=PIPE,stderr=PIPE)
	for line in cmdproc.stdout:
		print(line)

	"""
	subout = (Popen(shlex.split(findcmd),bufsize=4096,stdout=PIPE)).stdout
	subout = (Popen(findcmd.split(' '),bufsize=4096,stdout=PIPE)).stdout
	"""
	return s_rc


def subout4match(command_without_args, args=[], argnum=0, arg_regex=None):
	""" Suffix of 4 as we accept four args generally.
	cmdline should NOT contain any chr(124) pipe symbols (|)
	"""
	lines_feedback = []
	command_with_args = None
	s_rc = 0
	if args_valid(args):
		pass
	else:
		lines_feedback = [STRING_SUBOUT_ARGS_INVALID]
		s_rc = 202

	if arg_regex is not None:
		arg=args[argnum]
		#print("Testing arg={0}".format(arg))
		if arg_regex.match(arg):
			pass
		else:
			# 1+argnum so report back to the user is more natural (one indexed)
			feedback_failed = STRING_NTH_ARG_REGEX_FAILED_TEMPLATE.format(1+argnum)
			lines_feedback.append(feedback_failed)
			s_rc = 204

	if s_rc > 0:
		return (s_rc,[],[],lines_feedback,command_with_args)

	command_with_args = "{0} {1}".format(command_without_args,joined_from_list(args,' '))
	if PYVERBOSITY > 1:
		print(command_with_args)
	cmdproc = Popen(shlex.split(command_with_args),bufsize=BUFSIZE1, \
		stdout=PIPE,stderr=PIPE)
	lines = []
	for line in cmdproc.stdout:
		lines.append(line)
		#print(line)

	cmdproc.wait()
	s_rc = cmdproc.returncode
	if s_rc > 0:
		feedback_rc = STRING_NONZERO_RC_TEMPLATE.format(command_without_args,s_rc)
		lines_feedback.append(feedback_rc)
		if 1==s_rc and CMD_CHGRP==command_without_args:
			lines_feedback.append('Did you try to chgrp to an empty group?')
	#print(cmdproc.returncode)
	return (s_rc,lines,[],lines_feedback,command_with_args)


def subout4match_wrapper(command_without_args, args=[], argnum=0, arg_regex=None):
	""" Wrapper that will print lines_feedback when return code > 0 """
	s_rc, lines_out, lines_err, lines_feedback, command_with_args = \
		subout4match(command_without_args, args, argnum, arg_regex)
	if s_rc > 0:
		for line in lines_feedback:
			print(line)

	return (s_rc,lines_out,lines_err,lines_feedback,command_with_args)
	

def subout4splitn(command_without_args, args=[], argnum=0, arg_regex=None):
	""" Suffix of 4 as we accept four args generally.
	cmdline should NOT contain any chr(124) pipe symbols (|)
	"""
	command_with_args = "{0} {1}".format(command_without_args,joined_from_list(args,' '))
	s_rc = 0
	cmdproc = Popen(shlex.split(command_with_args),bufsize=BUFSIZE1, \
		stdout=PIPE,stderr=PIPE)
	lines = cmdproc.stdout.split('\n')
	for line in lines:
		print(line)

	return s_rc


if __name__ == '__main__':

	exit_rc = 0

	dir2create = None   
	if len(argv) > 1:
		dir2create = argv[1]
		#if len(argv) > 2:
                #else:
                #    exit_rc = 102
        else:
            exit_rc = 101

	if exit_rc > 0:
		exit(exit_rc)

	#subout4splitn_match(command_without_args, args=[], argnum=0, arg_regex=None)
	if PYVERBOSITY is None or PYVERBOSITY > 0:
		sub_rc, lines_out, lines_err, lines_feedback, _ = \
			subout4match_wrapper(CMD_MKDIR, [dir2create], 0, RE_ALPHA_UNDERSCORE)
	else:
		# Enable silent running by setting PYVERBOSITY=0 in your environment
		sub_rc, lines_out, lines_err, lines_feedback, _ = \
			subout4match(CMD_MKDIR, [dir2create], 0, RE_ALPHA_UNDERSCORE)

	if sub_rc > 0:
		exit(sub_rc)

	gid = None
	try:
		gid = grp.getgrnam(dir2create).gr_gid
	except KeyError:
		gid = -1
		sub_rc = 211

	if gid >= 0:
		#gid_pref = ":{0}".format(gid)
		sub_rc = 0
		if PYVERBOSITY is None or PYVERBOSITY > 0:
			sub_rc, lines_out, lines_err, lines_feedback, _ = \
				subout4match_wrapper(CMD_CHGRP, [CHGRP_DEREF,'-R',gid,dir2create], \
							3, RE_ALPHA_UNDERSCORE)
		else:
			sub_rc, lines_out, lines_err, lines_feedback, _ = \
				subout4match(CMD_CHGRP, [CHGRP_DEREF,'-R',gid,dir2create], \
							3, RE_ALPHA_UNDERSCORE)
	elif 0==PYVERBOSITY:
		pass
	elif PYVERBOSITY is None or PYVERBOSITY > 0:
		print("gid={0} has been set from Group lookup of {1} - no chgrp!".format(gid,dir2create))
	else:
		pass
		#sub_rc = 211

	if sub_rc > 0:
		exit(sub_rc)

	chmod(dir2create, CHMOD2)

	exit(sub_rc)

