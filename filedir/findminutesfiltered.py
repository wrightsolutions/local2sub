#!/usr/bin/env python
#
# python ./findminutesfiltered.py /tmp/ 30
# For much feedback on error before running execute export PYVERBOSITY=2
# Read regexes from two input files:
#  /etc/relocal/findminutesfiltered_match.re
#  /etc/relocal/findminutesfiltered_search.re
#
# We take a two arguments but could be expanded to 4 args in future to make explicit following:
# python ./findminutesfiltered.py /tmp/ 30 /etc/relocal/findminutesfiltered_match.re /etc/relocal/findminutesfiltered_search.re

""" Tested initially using Python 2.7.5
Intended to have up to 2 arguments minimum with a second optional argument

Both types match and search types of regex operation are supported.

Both types of regex specification are supported 'raw' and 'string' for each of above

Because this program will [initially] fail silently if there are errors
in your patterns, you might want to define a simple match or two in that standalone
match file and have your more complex patterns in the standalone search file.
That way some successful extracts but some missing likely indicates a problem or two
in the file of 'search' patterns.

Intended to be run as root OR on directories where you are sure you will never encounter permission denied

"""

#from collections import defaultdict
from copy import deepcopy
import fileinput
import gc
from os import getenv as osgetenv
from os import path as ospath
from os import sep
#from os import stat as osstat
import re
import select
#from signal import signal, SIGHUP, SIGINT, SIGKILL, SIGTERM
from string import printable,punctuation
from sys import argv,exit
from subprocess import Popen,PIPE
import shlex
import re

COLON = chr(58)
DOTTY = chr(46)
RE_BRACKETED_TIME_OR_NUM = re.compile(r'\[[\-T0-9.:]*\]')

SET_PRINTABLE=set(printable)
TRANSLATE_DELETE = ''.join(map(chr, range(0,9)))
#print(len(TRANSLATE_DELETE))

BUFSIZE1=4096
TABBY=chr(9)
UNDERSCORE=chr(95)
PARENTH_OPEN=chr(40)
PARENTH_CLOSE=chr(41)
DQ=chr(34)
SQ=chr(39)
PIPEY=chr(124)
CHMOD_UNPREFIXED=2770
CHMOD2=02770
#CHMOD3=0o2770

RE_NUMERIC_MARKER = re.compile('\d')

RE_ALPHA_UNDERSCORE = re.compile('[a-zA-Z]+{0}'.format(UNDERSCORE))
#RE_USERS_PID = re.compile(r"users{0}\{1}\{1}{2}.*{2},pid=".format(COLON,PARENTH_OPEN,DQ))
#RE_USERS_CSV = re.compile(r"users{0}{1}{1}<>{2}{2}".format(COLON,PARENTH_OPEN,PARENTH_CLOSE))

CMD_MKDIR='/usr/bin/mkdir'
CMD_CHGRP='/usr/bin/chgrp'
CHGRP_DEREF='--no-dereference'
CMD_RMDIR='/usr/bin/rmdir'
CMD_FIND='/usr/bin/find'

STRING_SUBOUT_ARGS_INVALID='Args list for subout is not valid - does it contain pipe (|)?'
STRING_NTH_ARG_REGEX_FAILED_TEMPLATE="Arg {0} did not pass the required regex test!"
STRING_NONZERO_RC_TEMPLATE="Command {0} gave non-zero returncode rc={1}"

MINS_PER_DAY = 1440
MINS_PER_DAY2 = 2*MINS_PER_DAY

FEEDBACK_PERMS_EVERY_FILE = 'Did you try to search a directory where you might not have permissions on every file?'

PYVERBOSITY_STRING = osgetenv('PYVERBOSITY')
PYVERBOSITY = 1
if PYVERBOSITY_STRING is None:
	pass
else:
	try:
		PYVERBOSITY = int(PYVERBOSITY_STRING)
	except:
		pass
#print(PYVERBOSITY)

matcher_list = []
mlist_of_fullpaths = []
searcher_list = []
slist_of_fullpaths = []

mpat_fullpath = None


def lines_joined(lines,joiner=chr(10)):
	""" chr(9) is tab  ; chr(10) is line feed ;
	chr(32) is space ; chr(61) is equals (=)
	"""
        joined = joiner.join(lines)
        return joined


def fullpathed(search_type,index1):
	""" 
	Final argument is called index1 as when fullpathed first being 1
	is easier to follow on filesystem for some.
	"""
	fullpathed = None
	if mpat_fullpath is None:
		return None
	type1 = None
	try:
		type1 = search_type[0]
	except:
		type1 = 'u'
	fullpath_to_append_to = mpat_fullpath
	if DOTTY == mpat_fullpath[0] and len(mpat_fullpath) > 1:
		fullpath_to_append_to = mpat_fullpath[1:]
	try:
		fullpathed = "{0}.{1}.{2}".format(fullpath_to_append_to,type1,index1)
	except:
		pass
	#ospath.join(blah,blah)
	return fullpathed


def close_all_fullpathed(list_of_fullpaths):
	closed_count = 0
	for fp in list_of_fullpaths:
		if fp is not None:
			try:
				fp.flush()
				fp.close()
				closed_count += 1
			except:
				pass
	return closed_count


def empty_if_unprintables(unfiltered):
	#stripped = unfiltered.strip()
	if set(unfiltered).issubset(SET_PRINTABLE):
		return unfiltered
	return ''


def line_less_n_bracket_pairs(line,remove_pairs_max=2,stripper='both'):
	match_end_list = []
	for match in RE_BRACKETED_TIME_OR_NUM.finditer(line):
		match_end_list.append(match.end())

        try:
                if 0 == len(match_end_list):
                        unstripped = line
                elif remove_pairs_max > len(match_end_list):
                        unstripped = line[match_end_list[-1]:]
                else:
                        unstripped = line[match_end_list[remove_pairs_max-1]:]
        except IndexError as e:
                unstripped = line

        if 'both' == stripper:
                stripped = unstripped.strip()
        else:
                stripped = unstripped.lstrip()

	return stripped


def regexlines_to_dict(search_type,lines,prefix=None):
        """
	For supplied search_type populate appropriate dict with regexes
	Lifted from previous work where search_types could be interleaved.
        """
        matcher_appends = 0
        searcher_appends = 0
	for line in lines:
                try:
                        pat_dict = {}
			raw_indicator_as_string = line[0]
			#print(raw_indicator_as_string)
			raw_or_string = int(raw_indicator_as_string)
			#print(raw_or_string)
                        pat_dict['raw_or_string'] = raw_or_string
			if 'match' == search_type and prefix is not None:
	                        pat_dict['pattern'] = "{0}{1}".format(prefix,line[1:].strip())
			else:
	                        pat_dict['pattern'] = line[1:].strip()
			#print(pat_dict['pattern'])
                        # Next is a placeholder for the compiled re
                        pat_dict['rec'] = None
                except:
                        pat_dict = {}

                if len(pat_dict) < 3:
                        continue

                if 'match' == search_type:
                        matcher_list.append(pat_dict)
                        matcher_appends+=1
                elif 'search' == search_type:
                        searcher_list.append(pat_dict)
                        searcher_appends+=1
                else:
                        pass

        return (matcher_appends+searcher_appends)


def patternfile_to_memory(search_type,patfile_fullpath,prefix=None):
        """ Read in the regexes.
        Optionally print the supplied list on completion
        chr(47) is forward slash (/) ; chr(46) is dot / period
        """
        if patfile_fullpath is None:
                return 0
        elif len(patfile_fullpath) < 2:
                return 0
        elif patfile_fullpath.endswith('.re'):
                pass
        else:
                if patfile_fullpath.count(chr(47)) < 2:
                        return 0

	with open(patfile_fullpath,'r') as rfile:
		if PYVERBOSITY is None or PYVERBOSITY > 1:
			print(search_type,patfile_fullpath)
		to_dict_ret = regexlines_to_dict(search_type,rfile.readlines(),prefix)
        return to_dict_ret


def list_of_dicts_compile(list_of_dicts):
        """ Returns an updated list_of_dicts where rec field
        should now contain a compiled regex.
        Where the list_of_dicts supplied does not seem right [too short]
        we simply return the original list_of_dicts unchanged.
        """
        if list_of_dicts is None or len(list_of_dicts) < 1:
                return list_of_dicts

        list_updated = deepcopy(list_of_dicts)

        for idx,re_dict in enumerate(list_of_dicts):
                pat = re_dict['pattern']
                raw_or_string = re_dict['raw_or_string']
                # Next save the compiled re in field 'rec'
                try:
                        # chr(34) double quote ; chr(39) single quote
                        if 0 == raw_or_string:
                                if 'erminate' in pat and PYVERBOSITY > 1:
                                        print("compiling 'raw' pattern={0}".format(pat))
                                rec = re.compile(r"""%s""" % (pat))
                                # Above r"""
                        else:
                                # Below """
                                rec = re.compile("""%s""" % (pat))
                except Exception as exc:
                        rec = 0
                        rec_message = "{0} {1}".format(type(exc),str(exc))
                        re_dict['rec_message'] = rec_message
                        print(pat,rec_message)

                re_dict['rec'] = rec

                #if rec is not None and rec > 0:
                list_updated[idx] = re_dict

        return list_updated


def list_print_rec_plus(list_of_dicts,printer=True):
        """ For each dict in the list
        print rec field and some supplementary fields
        Set printer=False if you just want the list of reporting lines
        returned to you.
        """
        list_of_lines = []
        for idx,re_dict in enumerate(list_of_dicts):
                rec = re_dict['rec']
                pat = re_dict['pattern']
                line = "Pattern {0} was a ".format(idx)
                line = "{0} pattern ... {1} ... which ".format(line,pat)
                rec_result = 'is None'
                if rec is None:
                        pass
                elif 0 == rec:
                        rec_result = 'is ZERO so failed'
                        if 'rec_message' in re_dict:
                                recmess = re_dict['rec_message']
                                rec_result = "{0} with {1}".format(rec_result,recmess)
                else:
                        rec_result = 'succeeded :)'
                line = "{0}{1}".format(line,rec_result)
                list_of_lines = line
                if printer is not True:
                        continue
                print(line)

        return list_of_lines


def process_lines(lines_given,linelimit=0,verbosity=0):
	line_rc = 999

	lines_filtered = lines_given[:]

	for idx,line in enumerate(lines_given):

		#print("processing line %d" % idx)
		line_rc = 0

		if not set(line).issubset(SET_PRINTABLE):
			pass
			#line_rc = 110
			#lines_with_problems += 1
			#print('line %s has one or more unprintables!' % idx)
			#continue


		for midx,rmatch in enumerate(matcher_list):
			line_less = line_less_n_bracket_pairs(line)
			"""
			print("Checking mpat={0} for line={1}".format(rmatch['pattern'],
										line_less))
			"""
			try:
                                #print(line_less)
				if rmatch['rec'].match(line_less):
					midx1=1+midx
					del lines_filtered[idx]
					if PYVERBOSITY is None or PYVERBOSITY > 1:
						#print(midx)
						if len(rmatch['pattern']) >= 10:
							print("match() success on pattern={0}".format(rmatch['pattern']))
			except AttributeError as e:
				print(rmatch['rec_message'])

		for sidx,rsearch in enumerate(searcher_list):
			if rsearch['rec'].search(line):
				sidx1=1+sidx
				del lines_filtered[idx]
				#print(sidx1,sidx1,rsearch['pattern'])
				if PYVERBOSITY is None or PYVERBOSITY > 1:
					print("search() success on pattern={0}".format(rsearch['pattern']))

		"""						
		line_translated = line.translate(None,TRANSLATE_DELETE)
		if len(line) != len(line_translated):
			line_rc = 120
			lines_with_problems += 1
			print('line %s has tab or other low range chr()' % idx)
		"""

		if linelimit > 0:
			if idx >= linelimit:
				return 200

	if verbosity > -1:
		print("process_lines() in was {0}".format(len(lines_given)))
		print("process_lines() after filtering is {0}".format(len(lines_filtered)))

	return lines_filtered


def joined_from_list(arglist,joinstr=' '):
	return joinstr.join([str(arg) for arg in arglist])


def isfile_and_positive_size(target_path_given):
    target_path = target_path_given.strip()
    if not ospath.isfile(target_path):
        return False
    if not osstat(target_path).st_size:
        return False
    return True


def dirname_split_lower(dirname,lowcase=True):
	dsplit = dirname.split('_')[-1]
	if len(dsplit) > 0:
		dres = str.lower(dsplit)
	else:
		dres = dsplit
	return dres


def args_valid(args_list):
	pipe_count = 0
	for arg in args_list:
		try:
			if PIPEY in arg:
				pipe_count+=1
				break
		except TypeError:
			pass

	return (pipe_count < 1)


def args_valid3(args=[], argnum=0, arg_regex=None):
	feedback_line = ''
	arg=args[argnum]
	#print("Testing arg={0}".format(arg))
	valid3rc = 0
	if arg_regex.match(arg):
		pass
	else:
		# 1+argnum so report back to the user is more natural (one indexed)
		feedback_line = STRING_NTH_ARG_REGEX_FAILED_TEMPLATE.format(1+argnum)
		valid3rc = 204
	return (valid3rc,feedback_line)


def subout2match(command_without_args, args=[]):
	""" Suffix of 2 as we accept two args generally.
	cmdline should NOT contain any chr(124) pipe symbols (|)
	"""
	lines_feedback = []
	command_with_args = None
	s_rc = 0
	if args_valid(args):
		pass
	else:
		lines_feedback = [STRING_SUBOUT_ARGS_INVALID]
		s_rc = 202

	if s_rc > 0:
		return (s_rc,[],[],lines_feedback,command_with_args)

	command_with_args = "{0} {1}".format(command_without_args,joined_from_list(args,' '))
	if PYVERBOSITY > 1:
		print(command_with_args)
	cmdproc = Popen(shlex.split(command_with_args),bufsize=BUFSIZE1, \
		stdout=PIPE,stderr=PIPE)
	lines = []
	for line in cmdproc.stdout:
		lines.append(line)
		#print(line)

	cmdproc.wait()
	s_rc = cmdproc.returncode
	if s_rc > 0:
		feedback_rc = STRING_NONZERO_RC_TEMPLATE.format(command_without_args,s_rc)
		lines_feedback.append(feedback_rc)
		if 1==s_rc and CMD_FIND==command_without_args:
			lines_feedback.append(FEEDBACK_PERMS_EVERY_FILE)
	#print(cmdproc.returncode)
	return (s_rc,lines,[],lines_feedback,command_with_args)


if __name__ == '__main__':

	exit_rc = 0

	dir2supplied = None 
	mins_int = MINS_PER_DAY2
	if len(argv) > 1:
		dir2supplied = argv[1]
		#if len(argv) > 2:
                #else:
                #    exit_rc = 102
                if len(argv) > 2:
                        # Second argument is number of MINUTES
                        mins = argv[2]
			try:
				mins_int = int(mins)
			except:
				mins_int = MINS_PER_DAY2
        else:
		exit_rc = 101

	if exit_rc > 0:
		exit(exit_rc)

	program_binary = argv[0].strip()


	sub_rc = 0
	#subout4splitn_match(command_without_args, args=[], argnum=0, arg_regex=None)
	mkdir_argslist = ['-m', CHMOD_UNPREFIXED, dir2supplied]
	lines_out = []
	lines_feedback = []
	"""
	arg_regex_rc,feedback_line = args_valid3(mkdir_argslist, 2, RE_ALPHA_UNDERSCORE)
	if arg_regex_rc > 0:
		sub_rc = arg_regex_rc
		lines_feedback.append(feedback_line)
	"""

	dir1 = dirname_split_lower(dir2supplied,True)
	dir_fs = "{0}/".format(dir1)
	if 0 == sub_rc:
		find_argslist = [dir_fs, '-type', 'f', '-mmin', "+{0}".format(mins_int)]
		if PYVERBOSITY is None or PYVERBOSITY > 0:
			if PYVERBOSITY > 1:
				print("find "+joined_from_list(find_argslist,' '))
			sub_rc, lines_out, lines_err, lines_feedback, _ = \
				subout2match(CMD_FIND, find_argslist)
		else:
			# Enable silent running by setting PYVERBOSITY=0 in your environment
			sub_rc, lines_out, lines_err, lines_feedback, _ = \
				subout2match(CMD_FIND, find_argslist)

	flines = []
	if 0 == sub_rc:
		flines = lines_out
	else:
		if PYVERBOSITY is None or PYVERBOSITY > 0:
			if PYVERBOSITY > 1 and len(lines_feedback) > 0:
				for line in lines_feedback:
					print(line)
		print("Exiting {0}".format(sub_rc))
		exit(sub_rc)

	log1 = None
        spat_fullpath = None
        mpat_fullpath = None
        mpat_fullpath = '/etc/relocal/findminutesfiltered_match.re'
        spat_fullpath = '/etc/relocal/findminutesfiltered_search.re'

        #print(log1,mpat_fullpath)
        if mpat_fullpath is None:
                exit(121)

        """ Have got this far so know we have a minimum of 2 arguments
        and a fullpath defined for mpat [ patterns to be used for match() ]
        """

        #mappended = patternfile_to_memory(mpat_fullpath,matcher_list)
        mappended = patternfile_to_memory('match',mpat_fullpath,dir_fs)
	mlist_of_fullpaths = [None] * mappended
        #sappended = patternfile_to_memory(spat_fullpath,searcher_list)
        sappended = patternfile_to_memory('search',spat_fullpath)
	slist_of_fullpaths = [None] * sappended
	if PYVERBOSITY is None or PYVERBOSITY > 0:
		print(mappended,sappended)

	""" Having read in the regexes we next use re.compile to form
	precompiled regexes for use when iterating over the text input
	"""
        print(dir2supplied,mins_int,len(matcher_list),len(searcher_list))
        matcher_list_compiled = list_of_dicts_compile(matcher_list)
	if PYVERBOSITY is None or PYVERBOSITY > 0:
		if PYVERBOSITY > 1:
			list_print_rec_plus(matcher_list_compiled,True)
        searcher_list_compiled = list_of_dicts_compile(searcher_list)
	if PYVERBOSITY is None or PYVERBOSITY > 0:
		if PYVERBOSITY > 1:
			list_print_rec_plus(searcher_list_compiled,True)

	close_all_fullpathed(mlist_of_fullpaths)
	close_all_fullpathed(slist_of_fullpaths)

	if PYVERBOSITY > 1:
		lines_filtered = process_lines(flines,0,PYVERBOSITY)
	else:
		lines_filtered = process_lines(flines,0,1)
	#lret = process_lines(flines,0,0)

	if PYVERBOSITY is None or PYVERBOSITY > 0:
		if PYVERBOSITY > 1:
			for line in lines_filtered:
				print(line)

	if sub_rc > 0:
		exit(sub_rc)

	#chmod(dir2supplied, CHMOD2)

	exit(sub_rc)

