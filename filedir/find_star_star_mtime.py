#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2019, Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

# python ./find_star_star_mtime.py 7 15
# For much feedback on error before running execute export PYVERBOSITY=2
# Arguments of 7 15 instructs to handoff to /bin/find with -mtime +7 -mtime -15
# Overengineered currently, but setup to in [planned] future use CMD_FIND3 and REGEX args of match

from os import getenv as osgetenv
from os import path as ospath
#from os import stat as osstat
from os import sep
from sys import argv,exit
#from time import time
#import logging
from string import punctuation
from subprocess import Popen,PIPE
import shlex
import re
#from __future__ import with_statement

__version__ = '0.1'

PYVERBOSITY_STRING = osgetenv('PYVERBOSITY')
PYVERBOSITY = 1
if PYVERBOSITY_STRING is None:
	pass
else:
	try:
		PYVERBOSITY = int(PYVERBOSITY_STRING)
	except:
		pass
#print(PYVERBOSITY)
BUFSIZE1=4096
TABBY=chr(9)
UNDERSCORE=chr(95)
COLON=chr(58)
DOTTY=chr(46)
#DOUBLE_DOT=chr(46)*2
PARENTH_OPEN=chr(40)
PARENTH_CLOSE=chr(41)
DQ=chr(34)
SQ=chr(39)
PIPEY=chr(124)

RE_NUMERIC_MARKER = re.compile('\d')

RE_ALPHA_UNDERSCORE = re.compile('[a-zA-Z]+{0}'.format(UNDERSCORE))
#RE_USERS_PID = re.compile(r"users{0}\{1}\{1}{2}.*{2},pid=".format(COLON,PARENTH_OPEN,DQ))
#RE_USERS_CSV = re.compile(r"users{0}{1}{1}<>{2}{2}".format(COLON,PARENTH_OPEN,PARENTH_CLOSE))
RE_DOT_JPEG = re.compile('{0}jpeg'.format(DOTTY))

CMD_FIND='/usr/bin/find'
CMD_FINDF='/usr/bin/find . -type f '
CMD_FIND3="""
find . -type f -name '*.*' -mtime +7 -mtime -15 -printf '%Cs %CY%Cm%Cd.%CH%CM\t%s\t%f\t"%f"\0' | tr '"\000' '"\n' | sort
"""
CMD_CHGRP='/usr/bin/chgrp'
CHGRP_DEREF='--no-dereference'

STRING_SUBOUT_ARGS_INVALID='Args list for subout is not valid - does it contain pipe (|)?'
STRING_NTH_ARG_REGEX_FAILED_TEMPLATE="Arg {0} did not pass the required regex test!"
STRING_NONZERO_RC_TEMPLATE="Command {0} gave non-zero returncode rc={1}"


def joined_from_list(arglist,joinstr=' '):
	return joinstr.join([str(arg) for arg in arglist])


def isfile_and_positive_size(target_path_given):
    target_path = target_path_given.strip()
    if not ospath.isfile(target_path):
        return False
    if not osstat(target_path).st_size:
        return False
    return True


def args_valid(args_list):
	""" Regex args validator for internal use rather than command line argument parsing. """
	pipe_count = 0
	for arg in args_list:
		try:
			if PIPEY in arg:
				pipe_count+=1
				break
		except TypeError:
			pass

	return (pipe_count < 1)


def args_valid3(args=[], argnum=0, arg_regex=None):
	""" Regex args validator for internal use rather than command line argument parsing. """
	feedback_line = ''
	arg=args[argnum]
	#print("Testing arg={0}".format(arg))
	valid3rc = 0
	if arg_regex.match(arg):
		pass
	else:
		# 1+argnum so report back to the user is more natural (one indexed)
		feedback_line = STRING_NTH_ARG_REGEX_FAILED_TEMPLATE.format(1+argnum)
		valid3rc = 204
	return (valid3rc,feedback_line)


def subout4(command_without_args, args=[], argnum=0, arg_regex=None):
	""" Suffix of 4 as we accept four args generally.
	cmdline should NOT contain any chr(124) pipe symbols (|)
	This function [currently] does nothing with args 3,4 so functionality same subout2()
	"""
	lines_feedback = []
	command_with_args = None
	s_rc = 0
	if args_valid(args):
		pass
	else:
		lines_feedback = [STRING_SUBOUT_ARGS_INVALID]
		s_rc = 202

	# We are not going to use arguments 3 and 4 in current implementation so comment out 
	"""
	if arg_regex is not None:
		arg_regex_rc,feedback_line = args_valid3(args, argnum, arg_regex)
		if arg_regex_rc > 0:
			s_rc = arg_regex_rc
			lines_feedback.append(feedback_line)
	"""

	if s_rc > 0:
		return (s_rc,[],[],lines_feedback,command_with_args)

	command_with_args = "{0} {1}".format(command_without_args,joined_from_list(args,' '))
	if PYVERBOSITY > 1:
		print(command_with_args)
	cmdproc = Popen(shlex.split(command_with_args),bufsize=BUFSIZE1, \
		stdout=PIPE,stderr=PIPE)
	lines = []
	for line in cmdproc.stdout:
		lines.append(line)
		#print(line)

	cmdproc.wait()
	s_rc = cmdproc.returncode
	if s_rc > 0:
		feedback_rc = STRING_NONZERO_RC_TEMPLATE.format(command_without_args,s_rc)
		lines_feedback.append(feedback_rc)
		if 1==s_rc and CMD_CHGRP==command_without_args:
			lines_feedback.append('Did you try to chgrp to an empty group?')
	#print(cmdproc.returncode)
	return (s_rc,lines,[],lines_feedback,command_with_args)


def subout4match(command_without_args, args=[], argnum=0, arg_regex=None):
	""" Suffix of 4 as we accept four args generally.
	cmdline should NOT contain any chr(124) pipe symbols (|)
	This function [currently] does nothing with args 3,4 so functionality same subout2()
	"""
	lines_feedback = []
	command_with_args = None
	s_rc = 0
	if args_valid(args):
		pass
	else:
		lines_feedback = [STRING_SUBOUT_ARGS_INVALID]
		s_rc = 202

	if arg_regex is not None:
		arg_regex_rc,feedback_line = args_valid3(args, argnum, arg_regex)
		if arg_regex_rc > 0:
			s_rc = arg_regex_rc
			lines_feedback.append(feedback_line)

	if s_rc > 0:
		return (s_rc,[],[],lines_feedback,command_with_args)

	command_with_args = "{0} {1}".format(command_without_args,joined_from_list(args,' '))
	if PYVERBOSITY > 1:
		print(command_with_args)
	cmdproc = Popen(shlex.split(command_with_args),bufsize=BUFSIZE1, \
		stdout=PIPE,stderr=PIPE)
	lines = []
	for line in cmdproc.stdout:
		lines.append(line)
		#print(line)

	cmdproc.wait()
	s_rc = cmdproc.returncode
	if s_rc > 0:
		feedback_rc = STRING_NONZERO_RC_TEMPLATE.format(command_without_args,s_rc)
		lines_feedback.append(feedback_rc)
		if 1==s_rc and CMD_CHGRP==command_without_args:
			lines_feedback.append('Did you try to chgrp to an empty group?')
	#print(cmdproc.returncode)
	return (s_rc,lines,[],lines_feedback,command_with_args)


def subout4_wrapper(command_without_args, args=[], argnum=0, arg_regex=None):
	""" Wrapper that will print returned lines when 0 return code.
	Will print lines_feedback when return code > 0
	"""
	s_rc, lines_out, lines_err, lines_feedback, command_with_args = \
		subout4(command_without_args, args, argnum, arg_regex)
	if s_rc > 0:
		for fline in lines_feedback:
			print(fline)
	else:
		for line in lines_out:
			print(line.rstrip())

	return (s_rc,lines_out,lines_err,lines_feedback,command_with_args)
	

def subout4match_wrapper(command_without_args, args=[], argnum=0, arg_regex=None):
	""" Wrapper that will print lines_feedback when return code > 0 """
	s_rc, lines_out, lines_err, lines_feedback, command_with_args = \
		subout4match(command_without_args, args, argnum, arg_regex)
	if s_rc > 0:
		for line in lines_feedback:
			print(line)

	return (s_rc,lines_out,lines_err,lines_feedback,command_with_args)
	

def subout4splitn(command_without_args, args=[], argnum=0, arg_regex=None):
	""" Suffix of 4 as we accept four args generally.
	cmdline should NOT contain any chr(124) pipe symbols (|)
	"""
	command_with_args = "{0} {1}".format(command_without_args,joined_from_list(args,' '))
	s_rc = 0
	cmdproc = Popen(shlex.split(command_with_args),bufsize=BUFSIZE1, \
		stdout=PIPE,stderr=PIPE)
	lines = cmdproc.stdout.split('\n')
	for line in lines:
		print(line)

	return s_rc


if __name__ == '__main__':

	exit_rc = 0

	mtime1 = 1
	mtime2 = 2
	if len(argv) > 1:
		mtime1 = argv[1]
		if len(argv) > 2:
			mtime2 = argv[2]
		else:
			exit_rc = 102
        else:
            exit_rc = 101
	""" Above needs replacing with parser variable populated by argparse
	before you start doing anything fancy like supplying patters on
	command line to be used as 4th arg to subout4match()
	"""

	if exit_rc > 0:
		exit(exit_rc)

	#subout4splitn_match(command_without_args, args=[], argnum=0, arg_regex=None)
	find_arglist = ['-mtime', '+'+mtime1, '-mtime', '-'+mtime2]
	if PYVERBOSITY is None or PYVERBOSITY > 0:
		if PYVERBOSITY > 1:
			print("find "+joined_from_list(find_arglist,' '))
		sub_rc, lines_out, lines_err, lines_feedback, _ = \
			subout4_wrapper(CMD_FINDF, find_arglist, 2, RE_DOT_JPEG)
	else:
		# Enable silent running by setting PYVERBOSITY=0 in your environment
		sub_rc, lines_out, lines_err, lines_feedback, _ = \
			subout4_wrapper(CMD_FINDF, find_arglist)
		# When want a regex match supporting variant use subout4match...() instead

	if sub_rc > 0:
		exit(sub_rc)

	exit(sub_rc)

